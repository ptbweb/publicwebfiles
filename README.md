## These are public files that can be used across PVDA/PTB sites regardless of theme.

Currently it contains our cookieconsent-settings.
Usage on site:

    <script src="https://bitbucket.org/ptbweb/publicwebfiles/raw/master/cookieconsent.min.js" data-cfasync="false"></script>
    <script  src="https://bitbucket.org/ptbweb/publicwebfiles/raw/master/cookieconsent-config-fr.js" ></script>

The first file is the open source cookie consent code from https://github.com/brainsum/cookieconsent - version 1.2.3.

The second is our custom configuration file, and can be updated based on the site language and cookie settings. Currently only two versions: NL/FR

## Hosting

The public webfiles will be hosted on our Combell server pvda-002.dienenvzw.

* Application **consent** under account **pvdastatic**
* Application folder: ~/apps/consent
* Docroot: ~/apps/consent/web
* Aliases:
    * consent.pvda.be
    * consent.ptb.be

## Usage examples

To include cookie consents on pvda sites.

```
<script src="https://consent.pvda.be/cookieconsent.min.js" data-cfasync="false"></script>
<script  src="https://consent.pvda.be/cookieconsent-config-nl.js" ></script>
```

To include cookie consents on ptb sites.

```
<script src="https://consent.ptb.be/cookieconsent.min.js" data-cfasync="false"></script>
<script  src="https://consent.ptb.be/cookieconsent-config-fr.js" ></script>
```


