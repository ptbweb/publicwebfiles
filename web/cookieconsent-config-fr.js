window.CookieConsent.init({
  // More link URL on bar
  modalMainTextMoreLink: null,
  // How lond to wait until bar comes up
  barTimeout: 1000,
  // Look and feel
  theme: {
    barColor: '#00b6be',
    barTextColor: '#FFF',
    barMainButtonColor: '#FFF',
    barMainButtonTextColor: '#542269',
    modalMainButtonColor: '#00b6be',
    modalMainButtonTextColor: '#FFF',
  },
  language: {
    // Current language
    current: 'fr-BE',
    locale: {
      'fr-BE': {
        barMainText: 'Les cookies sont nécessaires au bon fonctionnement de ce site.',
        barLinkSetting: 'Configuration des cookies',
        barBtnAcceptAll: 'Accepter tous',
        modalMainTitle: 'Configuration des cookies',
        modalMainText: 'Pour garantir le bon fonctionnement de notre site, la présence de cookies est parfois nécessaire. Les cookies sont de petits fichiers qui permettent notamment de ne pas devoir à chaque fois réintroduire vos données d’utilisateur.',
        modalBtnSave: 'Sauvegarder les paramètres actuels',
        modalBtnAcceptAll: 'Accepter et fermer',
        modalAffectedSolutions: 'Lié à:',
        learnMore: 'En savoir plus',
        on: 'Activé',
        off: 'Désactivé',
      }
    }
  },
  // List all the categories you want to display
  categories: {
    // Unique name
    // This probably will be the default category
    1: {
      // The cookies here are necessary and category cant be turned off.
      // Wanted config value  will be ignored.
      needed: true,
      // The cookies in this category will be let trough.
      // This probably should be false if not necessary category
      wanted: true,
      // If the checkbox is on or off at first run.
      checked: true,
      // Language settings for categories
      language: {
        locale: {
          'fr-BE': {
            name: 'Nécessaires',
            description: 'Les cookies nécessaires contribuent à rendre un site web utilisable en activant des fonctions de base comme la navigation de page et l\'accès aux zones sécurisées du site web. Le site web ne peut pas fonctionner correctement sans ces cookies.',
          }
        }
      }
    },
    2: {
      // The cookies here are necessary and category cant be turned off.
      // Wanted config value  will be ignored.
      needed: false,
      // The cookies in this category will be let trough.
      // This probably should be false if not necessary category
      wanted: true,
      // If the checkbox is on or off at first run.
      checked: true,
      // Language settings for categories
      language: {
        locale: {
          'fr-BE': {
            name: 'Préférences',
            description: 'Les cookies de préférences permettent à un site web de retenir des informations qui modifient la manière dont le site se comporte ou s’affiche, comme votre langue préférée ou la région dans laquelle vous vous situez.',
          }
        }
      }
    },
    3: {
      // The cookies here are necessary and category cant be turned off.
      // Wanted config value  will be ignored.
      needed: false,
      // The cookies in this category will be let trough.
      // This probably should be false if not necessary category
      wanted: true,
      // If the checkbox is on or off at first run.
      checked: true,
      // Language settings for categories
      language: {
        locale: {
          'fr-BE': {
            name: 'Statistiques',
            description: 'Les cookies statistiques aident les propriétaires du site web, par la collecte et la communication d\'informations de manière anonyme, à comprendre comment les visiteurs interagissent avec les sites web..',
         }
        }
      }
    },
    4: {
      // The cookies here are necessary and category cant be turned off.
      // Wanted config value  will be ignored.
      needed: false,
      // The cookies in this category will be let trough.
      // This probably should be false if not necessary category
      wanted: true,
      // If the checkbox is on or off at first run.
      checked: true,
      // Language settings for categories
      language: {
        locale: {
          'fr-BE': {
            name: 'Marketing',
            description: 'Les cookies marketing sont utilisés pour effectuer le suivi des visiteurs au travers des sites web. Le but est d\'afficher des publicités qui sont pertinentes et intéressantes pour l\'utilisateur individuel et donc plus précieuses pour les éditeurs et annonceurs tiers.',
          }
        }
      }
    },
    5: {
      // The cookies here are necessary and category cant be turned off.
      // Wanted config value  will be ignored.
      needed: false,
      // The cookies in this category will be let trough.
      // This probably should be false if not necessary category
      wanted: true,
      // If the checkbox is on or off at first run.
      checked: true,
      // Language settings for categories
      language: {
        locale: {
          'fr-BE': {
            name: 'Autres',
            description: 'des cookies qui n\'entrent dans aucune autre catégorie.',
          }
        }
      }
    },
    
  },
  // List actual services here
  services: {
    //start service paste here



    
    James:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-PC6QKN6',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: Ce cookie est utilisé par Facebook pour cibler des publicités en fonction du comportement et des préférences de lutilisateur d’un site web à l’autre. Le cookie contient un identifiant crypté qui permet à Facebook didentifier lutilisateur sur tous les sites web.'
          }
        }
      }
    },
   Robert:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-PC6QKN6',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: Utilisé par Facebook pour fournir une série de produits publicitaires tels que les offres en temps réel dannonceurs tiers.'
          }
        }
      }
    },
   John:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-PC6QKN6',
      cookies: [
        {
          // known cookie name.
          name: 'youtube.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'youtube.com: Enregistre un identifiant unique utilisé pour générer des données statistiques sur la façon dont le visiteur utilise le site.'
          }
        }
      }
    },
   Michael:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-PC6QKN6',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: Utilisé par Google Analytics our recueillir des données sur le nombre de fois quun utilisateur a visité le site web ainsi que les dates de la première et de la plus récente visite. '
          }
        }
      }
    },
   William:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-PC6QKN6',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: Utilisé par Google Analytics pour diminuer radicalement le taux de requêtes'
          }
        }
      }
    },
   David:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-PC6QKN6',
      cookies: [
        {
          // known cookie name.
          name: 'paperform.co',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'paperform.co: Enregistre un identifiant unique utilisé pour générer des données statistiques sur la façon dont le visiteur utilise le site.'
          }
        }
      }
    },
   Richard:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'doubleclick.net',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'doubleclick.net: '
          }
        }
      }
    },
   Joseph:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'cdn.embedly.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'cdn.embedly.com: '
          }
        }
      }
    },
   Thomas:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'g8vlmj57.paperform.co',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'g8vlmj57.paperform.co: '
          }
        }
      }
    },
   Charles:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'paperform.co',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'paperform.co: '
          }
        }
      }
    },
   Christopher:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: '
          }
        }
      }
    },
   Daniel:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: '
          }
        }
      }
    },
   Matthew:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'paperform.co',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'paperform.co: Identifiant unique qui identifie la session de lutilisateur.'
          }
        }
      }
    },
   Anthony:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: '123formbuilder.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: /^PHPSESSID.*$ /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: '123formbuilder.com: Assure la sécurité de la navigation des visiteurs en empêchant la falsification des requêtes entre sites. Ce cookie est essentiel pour la sécurité du site Web et du visiteur.'
          }
        }
      }
    },
   Mark:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: 'paperform.co',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'paperform.co: Collecte des données sur le comportement et linteraction des internautes, pour optimiser le site web et rendre la publicité sur le site plus pertinente.'
          }
        }
      }
    },
   Donald:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: 'g8vlmj57.paperform.co',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'g8vlmj57.paperform.co: Collecte des données sur le comportement et linteraction des internautes, pour optimiser le site web et rendre la publicité sur le site plus pertinente.'
          }
        }
      }
    },
   Steven:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: 'paperform.co',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'paperform.co: Collecte des données sur le comportement et linteraction des internautes, pour optimiser le site web et rendre la publicité sur le site plus pertinente.'
          }
        }
      }
    },
   Paul:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: '
          }
        }
      }
    },
   Andrew:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: /^_ga_[A-Z0-9]{10}$ /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: '
          }
        }
      }
    },
   Joshua:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.ptb.be/tags/ptbentourn_e',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: /^_gat(_.+)*$ /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: Recueille des données relatives aux visites de lutilisateur du site web, telles que le nombre de visites, le temps moyen passé sur le site et les pages qui ont été chargées, afin de personnaliser et daméliorer le service Twitter.'
          }
        }
      }
    },
   Kenneth:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: Collecte des données sur le comportement et linteraction des internautes, pour optimiser le site web et rendre la publicité sur le site plus pertinente.'
          }
        }
      }
    },
   Kevin:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-PC6QKN6',
      cookies: [
        {
          // known cookie name.
          name: 'google-analytics.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: /^[r\/]*collect$ /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'google-analytics.com: Utilisé pour envoyer des données à Google Analytics sur le périphérique et le comportement du visiteur. Suit linternaute à travers les appareils et les canaux de marketing.'
          }
        }
      }
    },
   Brian:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.youtube.com/embed/wtthxe1xn0u',
      cookies: [
        {
          // known cookie name.
          name: 'typekit.net',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'typekit.net: Utilisé pour détecter si le visiteur a accepté la catégorie marketing dans la bannière de cookie. Ce cookie est nécessaire pour la conformité du site Web avec le RGPD.'
          }
        }
      }
    },
   George:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://consent.cookiebot.com/uc.js',
      cookies: [
        {
          // known cookie name.
          name: 'twitter.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'twitter.com: Stocke lautorisation dutilisation de cookies pour le domaine actuel par lutilisateur '
          }
        }
      }
    },
   Edward:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: 'vimeo.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'vimeo.com: Ce cookie est utilisé pour déterminer si le visiteur a accepté la boite de consentement des cookies.'
          }
        }
      }
    },
   Ronald:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.ptb.be/tags/ptbentourn_e',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: Utilisé par Facebook pour détecter une activité suspecte de connexion.'
          }
        }
      }
    },
   Timothy:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: Identifie lutilisateur connecté. Un identifiant unique de session est lié à lutilisateur afin de pouvoir l’identifier lors de la navigation du site. Lutilisateur est déconnecté lorsque le cookie expire.'
          }
        }
      }
    },
   Jason:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: '
          }
        }
      }
    },
   Jeffrey:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: '
          }
        }
      }
    },
   Ryan:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: '
          }
        }
      }
    },
   Jacob:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://cdn.embedly.com/widgets/platform.js',
      cookies: [
        {
          // known cookie name.
          name: 'twitter.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'twitter.com: Collecte des données sur l’utilisation du système de commentaires par le visiteur sur le site web et les blogs / articles qu’il a lus. Cela peut être utilisé à des fins de marketing.'
          }
        }
      }
    },
   Gary:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: Collecte des données sur l’utilisation du système de commentaires par le visiteur sur le site web et les blogs / articles qu’il a lus. Cela peut être utilisé à des fins de marketing.'
          }
        }
      }
    },
   Nicholas:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.ptb.be/tags/ptbentourn_e',
      cookies: [
        {
          // known cookie name.
          name: 'facebook.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'facebook.com: Utilisé par Facebook pour fournir une série de produits publicitaires tels que les offres en temps réel dannonceurs tiers.'
          }
        }
      }
    },
   Eric:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/tags/ptbentourn_e',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: /^fbssls_\d+$ /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: Recueille des données relatives aux visites de lutilisateur du site web, telles que le nombre de visites, le temps moyen passé sur le site et les pages qui ont été chargées, afin de personnaliser et daméliorer le service Twitter.'
          }
        }
      }
    },
   Jonathan:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.ptb.be/tags/ptbentourn_e',
      cookies: [
        {
          // known cookie name.
          name: 'facebook.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'facebook.com: '
          }
        }
      }
    },
   Stephen:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://platform.twitter.com/widgets.js',
      cookies: [
        {
          // known cookie name.
          name: 'twitter.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'twitter.com: Définit un identifiant unique pour le visiteur, qui permet aux annonceurs tiers de cibler le visiteur avec une publicité pertinente. Ce service de couplage est fourni par des centres publicitaires tiers, ce qui facilite les enchères en temps réel pour les annonceurs. '
          }
        }
      }
    },
   Larry:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.youtube.com/embed/wtthxe1xn0u',
      cookies: [
        {
          // known cookie name.
          name: 'facebook.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'facebook.com: Utilisé par Google DoubleClick pour enregistrer et signaler les actions de lutilisateur du site après quil ait vu ou cliqué sur une des pubs de lannonceur dans le but de mesurer lefficacité et de présenter des annonces publicitaires ciblées à lutilisateur.'
          }
        }
      }
    },
   Justin:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'twitter.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'twitter.com: Ce cookie est utilisé en interne par les propriétaires du site lors du téléchargement ou du renouvèlement du contenu du site.'
          }
        }
      }
    },
   Scott:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'doubleclick.net',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'doubleclick.net: Ce cookie est utilisé en interne par les propriétaires du site lors du téléchargement ou du renouvèlement du contenu du site.'
          }
        }
      }
    },
   Brandon:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Utilisé dans le contexte de la publicité vidéo. Le cookie limite le nombre de fois quun même internaute a le même contenu publicitaire. Le cookie est également utilisé pour assurer la pertinence de la publicité vidéo pour linternaute spécifique.'
          }
        }
      }
    },
   Benjamin:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'ptb.be',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'ptb.be: Maintient les paramètres et les données de sortie lors de lutilisation de la Developer Tools Console sur la session en cours.'
          }
        }
      }
    },
   Samuel:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-PC6QKN6',
      cookies: [
        {
          // known cookie name.
          name: 'google.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'google.com: Utilisé dans le cadre de la fonction ChatBot sur le site web. Le cookie contient des données sur le comportement des utilisateurs, ce qui permet au site web de promouvoir des produits ou des services via la fonction ChatBot ou dautres messageries pop-up.'
          }
        }
      }
    },
   Gregory:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-PC6QKN6',
      cookies: [
        {
          // known cookie name.
          name: 'twitter.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'twitter.com: Identifie la dernière page visitée par le visiteur. Ceci est utilisé afin de rendre la fonction de chat plus pertinente.'
          }
        }
      }
    },
   Frank:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://apis.google.com/js/plusone.js',
      cookies: [
        {
          // known cookie name.
          name: 'facebook.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'facebook.com: Enregistre un identifiant qui identifie lappareil de lutilisateur récurrent. Cet identifiant est utilisé pour des annonces ciblées.'
          }
        }
      }
    },
   Alexander:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: 'facebook.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'facebook.com: Assure le suivi des polices spéciales utilisées sur le site Web pour une analyse interne. Le cookie nenregistre aucune donnée de visiteur.'
          }
        }
      }
    },
   Raymond:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'facebook.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'facebook.com: '
          }
        }
      }
    },
   Patrick:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/tags/ptbentourn_e',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Ce cookie est défini par Twitter - Le cookie permet a linternaute de partager le contenu du site web sur son profil Twitter.'
          }
        }
      }
    },
   Jack:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.123formbuilder.com/embed/4598598.js',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Conserve la configuration des paramètres des utilisateurs à travers les demandes de page.'
          }
        }
      }
    },
   Dennis:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'facebook.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'facebook.com: '
          }
        }
      }
    },
   Jerry:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'youtube.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'youtube.com: '
          }
        }
      }
    },
   Tyler:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.ptb.be/tags/ptbentourn_e',
      cookies: [
        {
          // known cookie name.
          name: 'facebook.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'facebook.com: Utilisé par Facebook pour fournir une série de produits publicitaires tels que les offres en temps réel dannonceurs tiers.'
          }
        }
      }
    },
   Aaron:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.ptb.be/tags/ptbentourn_e',
      cookies: [
        {
          // known cookie name.
          name: 'youtube.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'youtube.com: Définit un identifiant unique pour la session. Cela permet au site Web dobtenir des données sur le comportement des visiteurs à des fins statistiques.'
          }
        }
      }
    },
   Jose:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/tags/ptbentourn_e',
      cookies: [
        {
          // known cookie name.
          name: 'youtube.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'youtube.com: '
          }
        }
      }
    },
   Adam:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'youtube.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'youtube.com: '
          }
        }
      }
    },
   Henry:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'youtube.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'youtube.com: '
          }
        }
      }
    },
   Nathan:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'youtube.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'youtube.com: '
          }
        }
      }
    },
   Douglas:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'youtube.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'youtube.com: '
          }
        }
      }
    },
   Zachary:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.youtube.com/embed/wtthxe1xn0u',
      cookies: [
        {
          // known cookie name.
          name: 'youtube.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'youtube.com: Utilisé pour vérifier si le navigateur de lutilisateur accepte les cookies.'
          }
        }
      }
    },
   Peter:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'youtube.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'youtube.com: Utilisé dans le contexte de la publicité vidéo. Le cookie limite le nombre de fois quun même internaute a le même contenu publicitaire. Le cookie est également utilisé pour assurer la pertinence de la publicité vidéo pour linternaute spécifique.'
          }
        }
      }
    },
   Kyle:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/emma',
      cookies: [
        {
          // known cookie name.
          name: 'youtube.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'youtube.com: Utilisé dans le contexte de la publicité vidéo. Le cookie limite le nombre de fois quun même internaute a le même contenu publicitaire. Le cookie est également utilisé pour assurer la pertinence de la publicité vidéo pour linternaute spécifique.'
          }
        }
      }
    },
   Walter:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-PC6QKN6',
      cookies: [
        {
          // known cookie name.
          name: 'youtube.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'youtube.com: Utilisé par Facebook pour fournir une série de produits publicitaires tels que les offres en temps réel dannonceurs tiers.'
          }
        }
      }
    },
   Ethan:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.youtube.com/embed/wtthxe1xn0u',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Tente destimer la bande passante des utilisateurs sur des pages avec des vidéos YouTube intégrées.'
          }
        }
      }
    },
   Jeremy:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: '//cdn.embedly.com/widgets/media.html?src=https%3a%2f%2fplayer.vimeo.com%2fvideo%2f66468174%3fapp_id%3d122963dntp=1wmode=transparenturl=https%3a%2f%2fvimeo.com%2f66468174image=https%3a%2f%2fi.vimeocdn.com%2fvideo%2f450655061_1280.jpgkey=e23856ccc1f011e0b5e44040d3dc5c07type=text%2fhtmlschema=vimeo',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Recueille des données sur les visites de lutilisateur sur le site web, telles que les pages qui ont été consultées.'
          }
        }
      }
    },
   Harold:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/tags/ptbentourn_e',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Enregistre la taille et la résolution de lécran afin doptimiser laffichage du contenu sur lappareil de lutilisateur.'
          }
        }
      }
    },
   Keith:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Assure la sécurité de la navigation des visiteurs en empêchant la falsification des requêtes entre sites. Ce cookie est essentiel pour la sécurité du site Web et du visiteur.'
          }
        }
      }
    },
   Christian:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Assure la sécurité de la navigation des visiteurs en empêchant la falsification des requêtes entre sites. Ce cookie est essentiel pour la sécurité du site Web et du visiteur.'
          }
        }
      }
    },
   Roger:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.youtube.com/embed/wtthxe1xn0u',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Enregistre un identifiant unique pour conserver des statistiques sur les vidéos de YouTube vues par lutilisateur.'
          }
        }
      }
    },
   Noah:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'eko.com: Stocke les préférences de lecture vidéo de lutilisateur pour les vidéos YouTube incorporées'
          }
        }
      }
    },
   Gerald:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'eko.com: Stocke les préférences de lecture vidéo de lutilisateur pour les vidéos YouTube incorporées'
          }
        }
      }
    },
   Carl:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/lagrandecolere',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Stocke les préférences de lecture vidéo de lutilisateur pour les vidéos YouTube incorporées'
          }
        }
      }
    },
   Terry:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.ptb.be/lagrandecolere',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Stocke les préférences de lecture vidéo de lutilisateur pour les vidéos YouTube incorporées'
          }
        }
      }
    },
   Sean:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Stocke les préférences de lecture vidéo de lutilisateur pour les vidéos YouTube incorporées'
          }
        }
      }
    },
   Austin:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Stocke les préférences de lecture vidéo de lutilisateur pour les vidéos YouTube incorporées'
          }
        }
      }
    },
   Arthur:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Stocke les préférences de lecture vidéo de lutilisateur pour les vidéos YouTube incorporées'
          }
        }
      }
    },
   Lawrence:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.youtube.com/embed/wtthxe1xn0u',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Enregistre un identifiant unique pour conserver des statistiques sur les vidéos de YouTube vues par lutilisateur.'
          }
        }
      }
    },
   Jesse:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.youtube.com/embed/wtthxe1xn0u',
      cookies: [
        {
          // known cookie name.
          name: 'video.eko.com',
          // expected cookie domain 
          domain: `.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'fr-BE': {
            name: 'video.eko.com: Enregistre un identifiant unique pour conserver des statistiques sur les vidéos de YouTube vues par lutilisateur.'
          }
        }
      }
    },
   
   
    
   
   

    //end service paste above this line (last comma can be removed)
  }
});