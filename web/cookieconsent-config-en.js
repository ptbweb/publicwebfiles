window.CookieConsent.init({
  // More link URL on bar
  modalMainTextMoreLink: null,
  // How long to wait until bar comes up
  barTimeout: 1000,
  // Look and feel
  theme: {
    barColor: '#00b6be',
    barTextColor: '#FFF',
    barMainButtonColor: '#FFF',
    barMainButtonTextColor: '#542269',
    modalMainButtonColor: '#00b6be',
    modalMainButtonTextColor: '#FFF',
  },
  language: {
    // Current language
    current: 'en',
    locale: {
      en: {
        barMainText: 'This website uses cookies to ensure you get the best experience on our website.',
        barLinkSetting: 'Cookie Settings',
        barBtnAcceptAll: 'Accept all cookies',
        modalMainTitle: 'Cookie settings',
        modalMainText: 'Cookies are small piece of data sent from a website and stored on the user\'s computer by the user\'s web browser while the user is browsing. Your browser stores each message in a small file, called cookie. When you request another page from the server, your browser sends the cookie back to the server. Cookies were designed to be a reliable mechanism for websites to remember information or to record the user\'s browsing activity. \n\n Nederlandse uitleg over onze cookies: www.pvda.be/cookiebeleid. \n\n Politique de cookies en Français: www.ptb.be/politique_de_cookies',
        modalBtnSave: 'Save current settings',
        modalBtnAcceptAll: 'Accept all cookies and close',
        modalAffectedSolutions: 'Affected solutions:',
        learnMore: 'Learn More',
        on: 'On',
        off: 'Off',
      }
    }
  },
  // List all the categories you want to display
  categories: {
    // Unique name
    // This probably will be the default category
    1: {
      // The cookies here are necessary and category cant be turned off.
      // Wanted config value  will be ignored.
      needed: true,
      // The cookies in this category will be let trough.
      // This probably should be false if not necessary category
      wanted: true,
      // If the checkbox is on or off at first run.
      checked: true,
      // Language settings for categories
      language: {
        locale: {
          'en': {
            name: 'Essential',
            description: '',
          }
        }
      }
    },
    2: {
      // The cookies here are necessary and category cant be turned off.
      // Wanted config value  will be ignored.
      needed: false,
      // The cookies in this category will be let trough.
      // This probably should be false if not necessary category
      wanted: true,
      // If the checkbox is on or off at first run.
      checked: true,
      // Language settings for categories
      language: {
        locale: {
          'en': {
            name: 'Preferences',
            description: '',
          }
        }
      }
    },
    3: {
      // The cookies here are necessary and category cant be turned off.
      // Wanted config value  will be ignored.
      needed: false,
      // The cookies in this category will be let trough.
      // This probably should be false if not necessary category
      wanted: true,
      // If the checkbox is on or off at first run.
      checked: true,
      // Language settings for categories
      language: {
        locale: {
          'en': {
            name: 'Statistics',
            description: '',
          }
        }
      }
    },
    4: {
      // The cookies here are necessary and category cant be turned off.
      // Wanted config value  will be ignored.
      needed: false,
      // The cookies in this category will be let trough.
      // This probably should be false if not necessary category
      wanted: true,
      // If the checkbox is on or off at first run.
      checked: true,
      // Language settings for categories
      language: {
        locale: {
          'en': {
            name: 'Marketing',
            description: '',
          }
        }
      }
    },
    5: {
      // The cookies here are necessary and category cant be turned off.
      // Wanted config value  will be ignored.
      needed: false,
      // The cookies in this category will be let trough.
      // This probably should be false if not necessary category
      wanted: true,
      // If the checkbox is on or off at first run.
      checked: true,
      // Language settings for categories
      language: {
        locale: {
          'en': {
            name: 'Other',
            description: '',
          }
        }
      }
    },
    
  },
  // List actual services here
  services: {
    //start service paste here

    

    Genevieve:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-M9MPBBS',
      cookies: [
        {
          // known cookie name.
          name: '_fbc',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_fbc'
          }
        }
      }
    },
   Aurelia:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-M9MPBBS',
      cookies: [
        {
          // known cookie name.
          name: '_fbp',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_fbp: Gebruikt door Facebook om een reeks advertentieproducten te leveren, zoals realtime bieden van externe adverteerders.'
          }
        }
      }
    },
   Rose:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-M9MPBBS',
      cookies: [
        {
          // known cookie name.
          name: '_ga',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: /^_ga /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_ga: Registreert een uniek ID die wordt gebruikt om statistische gegevens te genereren over hoe de bezoeker de website gebruikt.'
          }
        }
      }
    },
   Eleanor:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-M9MPBBS',
      cookies: [
        {
          // known cookie name.
          name: '_ga_#',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: /^_ga_[A-Z0-9]{10}$ /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_ga_#: Verzamelt gegevens over het aantal keren dat een gebruiker de website heeft bezocht, evenals data voor het eerste en meest recente bezoek. Gebruikt door Google Analytics.'
          }
        }
      }
    },
   Violet:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-M9MPBBS',
      cookies: [
        {
          // known cookie name.
          name: '_gat',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: /^_gat(_.+)*$ /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_gat: Gebruikt door Google Analytics om verzoeksnelheid te vertragen'
          }
        }
      }
    },
   Luna:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-M9MPBBS',
      cookies: [
        {
          // known cookie name.
          name: '_gid',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_gid: Registreert een uniek ID die wordt gebruikt om statistische gegevens te genereren over hoe de bezoeker de website gebruikt.'
          }
        }
      }
    },
   Ada:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.pvda.be/forms/shares/new?page_id=4163',
      cookies: [
        {
          // known cookie name.
          name: '_nbuild_nocache',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_nbuild_nocache: '
          }
        }
      }
    },
   Evangeline:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: '_nbuild_session',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_nbuild_session: '
          }
        }
      }
    },
   Thea:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: '_s',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_s: Verzamelt gegevens over gedrag en interactie van bezoekers - Dit wordt gebruikt om de website te optimaliseren en om advertenties op de website relevanter te maken.'
          }
        }
      }
    },
   Khaleesi:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: '_shopify_s',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_shopify_s: Verzamelt gegevens over gedrag en interactie van bezoekers - Dit wordt gebruikt om de website te optimaliseren en om advertenties op de website relevanter te maken.'
          }
        }
      }
    },
   Lucy:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: '_shopify_y',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_shopify_y: Verzamelt gegevens over gedrag en interactie van bezoekers - Dit wordt gebruikt om de website te optimaliseren en om advertenties op de website relevanter te maken.'
          }
        }
      }
    },
   Mia:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.pvda.be/bedankt_kilometertaks_brussel',
      cookies: [
        {
          // known cookie name.
          name: '_twitter_sess',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_twitter_sess: Verzamelt gegevens met betrekking tot de bezoeken van de gebruiker aan de website, zoals het aantal bezoeken, de gemiddelde tijd die op de website is doorgebracht en welke paginas zijn geladen, met het doel de Twitter-service te personaliseren en te verbeteren.'
          }
        }
      }
    },
   Ivy:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: '_y',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_y: Verzamelt gegevens over gedrag en interactie van bezoekers - Dit wordt gebruikt om de website te optimaliseren en om advertenties op de website relevanter te maken.'
          }
        }
      }
    },
   Cora:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.pvda.be/onze_zorg_verdient_meer_dan_applaus_alleen',
      cookies: [
        {
          // known cookie name.
          name: 'cf_ob_info',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'cf_ob_info: Wordt gebruikt om te detecteren of de website ontoegankelijk is, in het geval van onderhoud van inhoudsupdates - De cookie staat de website toe om de bezoeker een kennisgeving over het probleem in kwestie te geven.'
          }
        }
      }
    },
   Amelia:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.pvda.be/onze_zorg_verdient_meer_dan_applaus_alleen',
      cookies: [
        {
          // known cookie name.
          name: 'cf_use_ob',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'cf_use_ob: Wordt gebruikt om te detecteren of de website ontoegankelijk is, in het geval van onderhoud van inhoudsupdates - De cookie staat de website toe om de bezoeker een kennisgeving over het probleem in kwestie te geven.'
          }
        }
      }
    },
   Ophelia:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-M9MPBBS',
      cookies: [
        {
          // known cookie name.
          name: 'collect',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: /^[r\/]*collect$ /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'collect: Gebruikt om gegevens naar Google Analytics te verzenden over het apparaat en het gedrag van de bezoeker. Traceert de bezoeker op verschillende apparaten en marketingkanalen.'
          }
        }
      }
    },
   Maia:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://cdn.embedly.com/widgets/media.html?src=https%3a%2f%2fwww.youtube.com%2fembed%2fmfipeiwbby4%3fwmode%3dtransparent%26feature%3doembedwmode=transparentdisplay_name=youtubeurl=https%3a%2f%2fwww.youtube.com%2fwatch%3fv%3dmfipeiwbby4image=https%3a%2f%2fi.ytimg.com%2fvi%2fmfipeiwbby4%2fhqdefault.jpgkey=e23856ccc1f011e0b5e44040d3dc5c07type=text%2fhtmlschema=youtube',
      cookies: [
        {
          // known cookie name.
          name: 'CONSENT',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'CONSENT: Gebruikt om te detecteren of de bezoeker de marketingcategorie in de cookie-banner heeft geaccepteerd. Deze cookie is nodig voor GDPR-naleving van de website.'
          }
        }
      }
    },
   Charlotte:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://consent.cookiebot.com/uc.js',
      cookies: [
        {
          // known cookie name.
          name: 'CookieConsent',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'CookieConsent: Slaat de cookiestatus van de gebruiker op voor het huidige domein'
          }
        }
      }
    },
   Isla:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: 'cookietest',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'cookietest: Deze cookie wordt gebruikt om vast te stellen of de bezoeker in het daarvoor bestemde vak toestemming voor cookies heeft gegeven.'
          }
        }
      }
    },
   Esme:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.pvda.be/tags/pvdaontour',
      cookies: [
        {
          // known cookie name.
          name: 'datr',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'datr: Gebruikt door Facebook om verdachte inlogactiviteiten te detecteren.'
          }
        }
      }
    },
   Stella:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'deviceId',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'deviceId: '
          }
        }
      }
    },
   Elise:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://cdn.embedly.com/widgets/platform.js',
      cookies: [
        {
          // known cookie name.
          name: 'fbssls_',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'fbssls_: '
          }
        }
      }
    },
   Jane:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: 'fbssls_#',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: /^fbssls_\d+$ /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'fbssls_#: Verzamelt gegevens over het gebruik van de commentsectie van de website door de bezoeker, en registreert bovendien welke blogs/artikelen de bezoeker heeft gelezen - Dit kan worden gebruikt voor marketingdoeleinden.'
          }
        }
      }
    },
   Maeve:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-M9MPBBS',
      cookies: [
        {
          // known cookie name.
          name: 'fr',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'fr: Gebruikt door Facebook om een reeks advertentieproducten te leveren, zoals realtime bieden van externe adverteerders.'
          }
        }
      }
    },
   Eloise:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.pvda.be/bedankt_kilometertaks_brussel',
      cookies: [
        {
          // known cookie name.
          name: 'guest_id',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'guest_id: Verzamelt gegevens met betrekking tot de bezoeken van de gebruiker aan de website, zoals het aantal bezoeken, de gemiddelde tijd die op de website is doorgebracht en welke paginas zijn geladen, met het doel de Twitter-service te personaliseren en te verbeteren.'
          }
        }
      }
    },
   Hazel:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: 'https://cx.atdmt.com/',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'https://cx.atdmt.com/: Stelt een unieke ID in voor de bezoeker, waarmee externe adverteerders de bezoeker kunnen targeten met relevante advertenties. Deze koppelservice wordt geleverd door advertentieshubs van derden, waardoor het realtime bieden voor adverteerders wordt vergemakkelijkt.'
          }
        }
      }
    },
   Claire:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://platform.twitter.com/widgets.js',
      cookies: [
        {
          // known cookie name.
          name: 'i/jot',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'i/jot: Stelt een unieke ID in voor de bezoeker, waarmee externe adverteerders de bezoeker kunnen targeten met relevante advertenties. Deze koppelservice wordt geleverd door advertentieshubs van derden, waardoor het realtime bieden voor adverteerders wordt vergemakkelijkt. '
          }
        }
      }
    },
   Imogen:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://cdn.embedly.com/widgets/media.html?src=https%3a%2f%2fwww.youtube.com%2fembed%2fmfipeiwbby4%3fwmode%3dtransparent%26feature%3doembedwmode=transparentdisplay_name=youtubeurl=https%3a%2f%2fwww.youtube.com%2fwatch%3fv%3dmfipeiwbby4image=https%3a%2f%2fi.ytimg.com%2fvi%2fmfipeiwbby4%2fhqdefault.jpgkey=e23856ccc1f011e0b5e44040d3dc5c07type=text%2fhtmlschema=youtube',
      cookies: [
        {
          // known cookie name.
          name: 'IDE',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'IDE: Gebruikt door Google DoubleClick om de acties van de websitegebruiker te registreren en te rapporteren na het bekijken of klikken op een van de advertenties van de adverteerder met het doel de effectiviteit van een advertentie te meten en om gerichte advertenties aan de gebruiker te presenteren.'
          }
        }
      }
    },
   Amara:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'laravel_session',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'laravel_session: Deze cookie wordt intern door de eigenaren van de website gebruikt wanneer inhoud van een website wordt geüpload of vernieuwd.'
          }
        }
      }
    },
   Rumi:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'laravel_session',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'laravel_session: Deze cookie wordt intern door de eigenaren van de website gebruikt wanneer inhoud van een website wordt geüpload of vernieuwd.'
          }
        }
      }
    },
   Emma:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-M9MPBBS',
      cookies: [
        {
          // known cookie name.
          name: 'mc_pixel_session_data',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'mc_pixel_session_data: Gebruikt in context met de ChatBot-functie op de website. De cookie bevat gegevens over gebruikersgedrag, waardoor de website producten of diensten kan promoten via de ChatBot-functie of andere pop-upberichten.'
          }
        }
      }
    },
   Evelyn:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-M9MPBBS',
      cookies: [
        {
          // known cookie name.
          name: 'mcwidget',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'mcwidget: Identificeert de laatst door de bezoeker bezochte pagina. Dit wordt gebruikt om de chatfunctie relevanter te maken.'
          }
        }
      }
    },
   Adeline:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://apis.google.com/js/plusone.js',
      cookies: [
        {
          // known cookie name.
          name: 'NID',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'NID: Registreert een uniek ID die het apparaat van een terugkerende gebruiker identificeert. Het ID wordt gebruikt voor gerichte advertenties.'
          }
        }
      }
    },
   Isabella:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'inline',
      cookies: [
        {
          // known cookie name.
          name: 'p.gif',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'p.gif: Houdt speciale lettertypen bij die op de website worden gebruikt voor interne analyse. De cookie registreert geen bezoekersgegevens.'
          }
        }
      }
    },
   Isabel:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'partialSaveId',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'partialSaveId: '
          }
        }
      }
    },
   Arabella:  {
      category: '3',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.pvda.be/bedankt_kilometertaks_brussel',
      cookies: [
        {
          // known cookie name.
          name: 'personalization_id',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'personalization_id: Deze cookie wordt ingesteld door Twitter - Met de cookie kan de bezoeker inhoud van de website delen op zijn Twitter-profiel.'
          }
        }
      }
    },
   Aurora:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.123formbuilder.com/embed/4598302.js',
      cookies: [
        {
          // known cookie name.
          name: 'PHPSESSID',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: /^PHPSESSID.*$ /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'PHPSESSID: Houdt gebruikerssessiestatus voor alle pagina-aanvragen bij.'
          }
        }
      }
    },
   Scarlett:  {
      category: '2',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'saved:5eaff2fe7a95b662d962d1c3',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'saved:5eaff2fe7a95b662d962d1c3: '
          }
        }
      }
    },
   Elizabeth:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.pvda.be/tags/pvdaontour',
      cookies: [
        {
          // known cookie name.
          name: 'Session',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'Session: Stelt een unieke ID in voor de sessie. Hierdoor kan de website voor statistische doeleinden gegevens over bezoekersgedrag verkrijgen.'
          }
        }
      }
    },
   Olivia:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://cdn.embedly.com/widgets/media.html?src=https%3a%2f%2fwww.youtube.com%2fembed%2fmfipeiwbby4%3fwmode%3dtransparent%26feature%3doembedwmode=transparentdisplay_name=youtubeurl=https%3a%2f%2fwww.youtube.com%2fwatch%3fv%3dmfipeiwbby4image=https%3a%2f%2fi.ytimg.com%2fvi%2fmfipeiwbby4%2fhqdefault.jpgkey=e23856ccc1f011e0b5e44040d3dc5c07type=text%2fhtmlschema=youtube',
      cookies: [
        {
          // known cookie name.
          name: 'test_cookie',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'test_cookie: Gebruikt om te controleren of de browser van de gebruiker cookies ondersteunt.'
          }
        }
      }
    },
   Alice:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.googletagmanager.com/gtm.js?id=GTM-M9MPBBS',
      cookies: [
        {
          // known cookie name.
          name: 'tr',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'tr: Gebruikt door Facebook om een reeks advertentieproducten te leveren, zoals realtime bieden van externe adverteerders.'
          }
        }
      }
    },
   Emilia:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://cdn.embedly.com/widgets/media.html?src=https%3a%2f%2fwww.youtube.com%2fembed%2fmfipeiwbby4%3fwmode%3dtransparent%26feature%3doembedwmode=transparentdisplay_name=youtubeurl=https%3a%2f%2fwww.youtube.com%2fwatch%3fv%3dmfipeiwbby4image=https%3a%2f%2fi.ytimg.com%2fvi%2fmfipeiwbby4%2fhqdefault.jpgkey=e23856ccc1f011e0b5e44040d3dc5c07type=text%2fhtmlschema=youtube',
      cookies: [
        {
          // known cookie name.
          name: 'VISITOR_INFO1_LIVE',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'VISITOR_INFO1_LIVE: Probeert de bandbreedte van gebruikers te schatten op paginas met geïntegreerde YouTube-videos.'
          }
        }
      }
    },
   Audrey:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://www.pvda.be/tags/pvdaontour',
      cookies: [
        {
          // known cookie name.
          name: 'wd',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'wd: Slaat de schermgrootte en resolutie op om de weergave van inhoud op het apparaat van de gebruiker te optimaliseren.'
          }
        }
      }
    },
   Ava:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'XSRF-TOKEN',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'XSRF-TOKEN: Zorgt voor browsingveiligheid, door vervalsing van cross-site verzoeken te voorkomen. Deze cookie is essentieel voor de veiligheid van de website en bezoeker.'
          }
        }
      }
    },
   Penelope:  {
      category: '1',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://paperform.co/__embed',
      cookies: [
        {
          // known cookie name.
          name: 'XSRF-TOKEN',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'XSRF-TOKEN: Zorgt voor browsingveiligheid, door vervalsing van cross-site verzoeken te voorkomen. Deze cookie is essentieel voor de veiligheid van de website en bezoeker.'
          }
        }
      }
    },
   Nora:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'https://cdn.embedly.com/widgets/media.html?src=https%3a%2f%2fwww.youtube.com%2fembed%2fmfipeiwbby4%3fwmode%3dtransparent%26feature%3doembedwmode=transparentdisplay_name=youtubeurl=https%3a%2f%2fwww.youtube.com%2fwatch%3fv%3dmfipeiwbby4image=https%3a%2f%2fi.ytimg.com%2fvi%2fmfipeiwbby4%2fhqdefault.jpgkey=e23856ccc1f011e0b5e44040d3dc5c07type=text%2fhtmlschema=youtube',
      cookies: [
        {
          // known cookie name.
          name: 'YSC',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'YSC: Registreert een unieke ID om statistieken bij te houden van welke videos van YouTube de gebruiker heeft gezien.'
          }
        }
      }
    },
   Iris:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'yt-remote-cast-available',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'yt-remote-cast-available: Bewaart de voorkeuren van de videospeler van de gebruiker  met ingesloten YouTube-video'
          }
        }
      }
    },
   Lydia:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'yt-remote-cast-installed',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'yt-remote-cast-installed: Bewaart de voorkeuren van de videospeler van de gebruiker  met ingesloten YouTube-video'
          }
        }
      }
    },
   Anna:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.pvda.be/degrotecolere',
      cookies: [
        {
          // known cookie name.
          name: 'yt-remote-connected-devices',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'yt-remote-connected-devices: Bewaart de voorkeuren van de videospeler van de gebruiker  met ingesloten YouTube-video'
          }
        }
      }
    },
   Astrid:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'https://www.pvda.be/degrotecolere',
      cookies: [
        {
          // known cookie name.
          name: 'yt-remote-device-id',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'yt-remote-device-id: Bewaart de voorkeuren van de videospeler van de gebruiker  met ingesloten YouTube-video'
          }
        }
      }
    },
   Lila:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'yt-remote-fast-check-period',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'yt-remote-fast-check-period: Bewaart de voorkeuren van de videospeler van de gebruiker  met ingesloten YouTube-video'
          }
        }
      }
    },
   Julia:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'yt-remote-session-app',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'yt-remote-session-app: Bewaart de voorkeuren van de videospeler van de gebruiker  met ingesloten YouTube-video'
          }
        }
      }
    },
   Sadie:  {
      category: '4',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'localcookie',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: 'yt-remote-session-name',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: 'yt-remote-session-name: Bewaart de voorkeuren van de videospeler van de gebruiker  met ingesloten YouTube-video'
          }
        }
      }
    },
   Caroline:  {
      category: '5',
      // can be: dynamic-script, script-tag, wrapped, localcookie
      type: 'dynamic-script',
      search: 'webserver',
      cookies: [
        {
          // known cookie name.
          name: '_nbuild_token',
          // expected cookie domain 
          domain:`.${window.location.hostname}`
        },
        {
          // regex matching cookie name.
          name: / /,
          domain: `.${window.location.hostname}`
        }
      ],
      language: {
        locale: {
          'en': {
            name: '_nbuild_token: '
          }
        }
      }
    },
   
   
   
   
   

    //end service paste above this line (last comma can be removed)
  }
});